package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
)

var clients []net.Conn

func main() {
	listener, err := net.Listen("tcp", "127.0.0.1:8080")
	if err != nil {
		log.Fatal("tcp server listener error:", err)
	}

	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Fatal("tcp server accept error", err)
		}
		clients = append(clients, conn)
		go handleConnection(conn)
	}
}

func sendMessage(conn net.Conn, msg string) {
	for i := range clients {
		if clients[i] != conn {
			clients[i].Write([]byte(msg))
		}
	}
}

func handleConnection(conn net.Conn) {

	bufferBytes, err := bufio.NewReader(conn).ReadBytes('\n')

	if err != nil {
		log.Println("client left..")
		conn.Close()
		return
	}

	message := string(bufferBytes)
	clientAddr := conn.RemoteAddr().String()
	response := fmt.Sprintf(message + "( from " + clientAddr + ")\n")

	log.Println(response)
	go sendMessage(conn, response)
	//conn.Write([]byte("you sent: " + response))

	handleConnection(conn)
}
