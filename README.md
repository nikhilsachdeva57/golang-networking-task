# GoLang-Networking-Task
It took me 2-3 days to research and build the required TCP server given that I am new to Golang networking libraries. I have worked with REST APIs and handled Networking in other languages(C++). <br>

Features :-
1. TCP server hosted at public IP
2. Multiple clients can join the public IP to communicate with the server.
3. They can forward messages to the other clients via their channels and vice versa receive messages as well.
4. The clients are identified using their Remote Address.

Screenshots :- 

Run the TCP server<br>
![Screenshot 2021-03-04 at 2 10 05 AM](https://user-images.githubusercontent.com/34986121/109869702-3b2e7500-7c8f-11eb-9bc6-a48ba4713857.png)

Join using netcat<br>
![Screenshot 2021-03-04 at 2 10 23 AM](https://user-images.githubusercontent.com/34986121/109869706-3c5fa200-7c8f-11eb-92bc-9035a63ab01f.png)

Send and receive messages<br>
![Screenshot 2021-03-04 at 2 11 00 AM](https://user-images.githubusercontent.com/34986121/109869642-2b169580-7c8f-11eb-8186-57df38d07265.png)
